/**
 * @jest-environment jsdom
 */

import {
  waitFor,
  waitForElementToBeRemoved,
  getByText,
} from '@testing-library/dom';

import * as axiosModule from '../scripts/libs/axios.min.js';

jest.mock('../scripts/libs/axios.min.js');

function setUpDom() {
  // Set up dom manually
  document.body.innerHTML = `
      <div id="notification" class="do-not-display"></div>
      
      <div class="input-section">
        <input type="text" id="vendor" />
        <input type="text" id="price" />
        <button id="add-offering">Hinzufügen</button>

        <select id="select-vendor"></select>
        <input type="text" id="new-price" />
        <button id="change-price">Ändern</button>
      </div>

      <table id="price-table">
         <thead>
           <tr>
             <th>Anbieter</th>
             <th>Preis</th>
             <th></th>
           </tr>
         </thead>
         <tbody></tbody>
      </table>`;
}

function prepareLoadOfOfferings(loadedOfferings, isRejected = false) {
  return initMain(loadedOfferings, isRejected);
}

function initMain(loadedOfferings, isRejected = false) {
  if (!isRejected) {
    axiosModule.get.mockResolvedValue({ data: loadedOfferings });
  } else {
    axiosModule.get.mockRejectedValue();
  }
  global.axios = axiosModule;
  return import('../scripts/main').then(() => {
    if (!isRejected && loadedOfferings.length > 0) {
      // Laden von Angeboten (asynchroner Prozess) abwarten
      return waitFor(() =>
        getByText(
          document.querySelector('#price-table tbody'),
          loadedOfferings[0].vendor
        )
      );
    }
  });
}

describe('Adding, deleting and updating offerings', () => {
  beforeEach(() => {
    setUpDom();
  });

  afterEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });

  function prepareAddOfOffering(addedOffering) {
    axiosModule.post.mockResolvedValue({ data: addedOffering });
    return initMain([]);
  }

  function prepareChangeOfOffering(changedOffering, loadedOfferings) {
    axiosModule.put.mockResolvedValue({ data: changedOffering });
    return initMain(loadedOfferings);
  }

  function prepareRemovalOfOffering(deletedOffering, loadedOfferings) {
    axiosModule.delete.mockResolvedValue({ data: deletedOffering });
    return initMain(loadedOfferings);
  }

  function getDeleteButton(offeringToDelete) {
    const elRows = document.querySelectorAll('#price-table tbody tr');
    for (let i = 0; i < elRows.length; i++) {
      const elRow = elRows[i];
      const elTds = elRow.getElementsByTagName('td');
      if (
        elTds.length === 3 &&
        elTds[0].textContent === offeringToDelete.vendor &&
        elTds[1].textContent === '' + offeringToDelete.price
      ) {
        return elRow.querySelector('button');
      }
    }
    return null;
  }

  function checkOfferingList(expectedOfferings) {
    const elRows = document.querySelectorAll('#price-table tbody tr');
    // check number of rows in offerings table
    expect(elRows.length).toBe(expectedOfferings.length);
    // check that correct offerings are added to the table
    for (let i = 0; i < elRows.length; i++) {
      const elRow = elRows[i];
      const expectedOffering = expectedOfferings[i];
      expect(elRow.getElementsByTagName('td')[0].textContent).toBe(
        expectedOffering.vendor
      );
      expect(elRow.getElementsByTagName('td')[1].textContent).toBe(
        expectedOffering.price
      );
    }
  }

  test('loading offerings successfully', async () => {
    // given  - Nichts zu tun
    // when - Vorbereiten von Laden von Angeboten
    const loadedOfferings = [
      { id: '1', vendor: 'orellfuessli.ch', price: 45 },
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    await prepareLoadOfOfferings(loadedOfferings);
    // then
    const expectedOfferings = [
      { vendor: 'saxbooks.ch', price: '42' },
      { vendor: 'orellfuessli.ch', price: '45' },
      { vendor: 'fontis.ch', price: '53' },
    ];
    checkOfferingList(expectedOfferings);
  });

  test('adding new offering successfully', async () => {
    // given
    // 1. Vorbereiten von Hinzufügen von Angebot
    const addedOffering = { id: '1', vendor: 'orellfuessli.ch', price: 45 };
    await prepareAddOfOffering(addedOffering);
    // 2. Daten für neues Angebot Eingabefeldern hinzufügen
    document.getElementById('vendor').value = addedOffering.vendor;
    document.getElementById('price').value = addedOffering.price;
    // when
    // 1. Klicken auf Button für Hinzufügen von Angebot
    const elAddBtn = document.getElementById('add-offering');
    elAddBtn.click();
    // 2. Warten bis Liste von Angeboten angepasst ist
    await waitFor(() =>
      getByText(
        document.querySelector('#price-table tbody'),
        addedOffering.vendor
      )
    );
    // then
    const expectedOfferings = [{ vendor: 'orellfuessli.ch', price: '45' }];
    checkOfferingList(expectedOfferings);
  });

  test('change price of offering successfully', async () => {
    // given
    // 1. Vorbereiten von Änderung von Angebot
    //    (und Laden von Angeboten)
    const changedOffering = { id: '3', vendor: 'fontis.ch', price: 25 };
    const offeringsToLoad = [
      { id: '1', vendor: 'orellfuessli.ch', price: 45 },
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    await prepareChangeOfOffering(changedOffering, offeringsToLoad);
    // 2. Angebot auswählen und neuen Preis Eingabefeld hinzufügen
    const elOptionToSel = document.querySelector(
      `#select-vendor option[value='${changedOffering.id}']`
    );
    elOptionToSel.selected = true;
    document.getElementById('new-price').value = changedOffering.price;
    // when
    // 1. Klicken auf Button für Preisänderung
    const elChangeBtn = document.getElementById('change-price');
    elChangeBtn.click();
    // 2. Warten bis Liste von Angeboten angepasst ist
    await waitFor(() =>
      getByText(document.querySelector('#price-table tbody'), '25')
    );
    // then
    const expectedOfferings = [
      { vendor: 'fontis.ch', price: '25' },
      { vendor: 'saxbooks.ch', price: '42' },
      { vendor: 'orellfuessli.ch', price: '45' },
    ];
    checkOfferingList(expectedOfferings);
  });

  test('delete offering successfully', async () => {
    // given
    // 1. Vorbereiten von Löschen von Angebot
    //    (und Laden von Angeboten)
    const deletedOffering = { id: '1', vendor: 'orellfuessli.ch', price: 45 };
    const offeringsToLoad = [
      deletedOffering,
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    await prepareRemovalOfOffering(deletedOffering, offeringsToLoad);
    // when
    // 1. Klicken auf Button für Löschen von Angebot
    const elDelBtn = getDeleteButton(deletedOffering);
    elDelBtn.click();
    // 2. Warten bis Liste von Angeboten angepasst ist
    await waitForElementToBeRemoved(() =>
      getByText(
        document.querySelector('#price-table tbody'),
        deletedOffering.vendor
      )
    );
    // then
    const expectedOfferings = [
      { vendor: 'saxbooks.ch', price: '42' },
      { vendor: 'fontis.ch', price: '53' },
    ];
    checkOfferingList(expectedOfferings);
  });
});

describe('Notification when loading offering', () => {
  beforeEach(() => {
    setUpDom();
  });

  afterEach(() => {
    jest.resetModules();
    jest.resetAllMocks();
  });

  function checkNotification(expectedMsg, expectedClass) {
    const elNotification = document.getElementById('notification');
    // Wird Benachrichtigung angezeigt?
    const classes = elNotification.classList;
    expect(classes.contains('do-not-diplayed')).toBeFalsy();
    // Ist Styling von Benachrichtigung korrekt?
    expect(classes.contains(expectedClass)).toBeTruthy();
    // Ist es die korrekte Benachrichtigung?
    expect(elNotification.textContent).toBe(expectedMsg);
  }

  test('Correctly notified for loading offerings successfully', async () => {
    // given  - Nichts zu tun
    // when - Vorbereiten von Laden von Angeboten
    const loadedOfferings = [
      { id: '1', vendor: 'orellfuessli.ch', price: 45 },
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    await prepareLoadOfOfferings(loadedOfferings);
    // then
    const expectedNotification = 'Angebote erfolgreich geladen';
    const expectedClass = 'info';
    checkNotification(expectedNotification, expectedClass);
  });

  test('Correctly notified for loading offerings unsuccessfully', async () => {
    // given  - Nichts zu tun
    // when - Vorbereiten von Laden von Angeboten
    const loadedOfferings = [
      { id: '1', vendor: 'orellfuessli.ch', price: 45 },
      { id: '2', vendor: 'saxbooks.ch', price: 42 },
      { id: '3', vendor: 'fontis.ch', price: 53 },
    ];
    await prepareLoadOfOfferings(loadedOfferings, true);
    // then
    const expectedNotification = 'Angebote konnten nicht geladen werden';
    const expectedClass = 'error';
    checkNotification(expectedNotification, expectedClass);
  });
});
