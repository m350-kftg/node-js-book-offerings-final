import {
  addInfoNotification,
  addErrorNotification,
  clearNotification,
} from './notifications.js';

const baseUrl = '/api/offerings';

const getAll = () => {
  clearNotification();
  const request = axios.get(baseUrl);
  return handleResponse(
    request,
    'Angebote erfolgreich geladen',
    'Angebote konnten nicht geladen werden'
  );
};

const create = (newObject) => {
  clearNotification();
  const request = axios.post(baseUrl, newObject);
  return handleResponse(
    request,
    'Neues Angebot erfolgreich erstellt',
    'Neues Angebot konnte nicht erstellt werden'
  );
};

const update = (id, newObject) => {
  clearNotification();
  const request = axios.put(`${baseUrl}/${id}`, newObject);
  return handleResponse(
    request,
    'Angebot erfolgreich angepasst',
    'Angebot konnte nicht angepasst werden'
  );
};

const remove = (id) => {
  clearNotification();
  const request = axios.delete(`${baseUrl}/${id}`);
  return handleResponse(
    request,
    'Angebot erfolgreich gelöscht',
    'Angebot konnte nicht gelöscht werden'
  );
};

const handleResponse = (request, infoMsg, errMsg) => {
  return request
    .then((response) => {
      addInfoNotification(infoMsg);
      return response.data;
    })
    .catch((error) => {
      console.error(error);
      addErrorNotification(errMsg);
      return [];
    });
};

export default { getAll, create, update, remove };
