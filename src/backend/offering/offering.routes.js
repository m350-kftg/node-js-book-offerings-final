import { Router } from 'express';
import {
  getOfferings,
  createOffering,
  updateOffering,
  removeOffering,
} from './offering.controller.js';

const router = Router();

router.get('/', getOfferings);
router.post('/', createOffering);
router.put('/:id', updateOffering);
router.delete('/:id', removeOffering);

export { router };
